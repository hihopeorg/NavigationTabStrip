/*
 * Copyright (C) 2015 Basil Miller
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gigamole.navigationtabstrip;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.multimodalinput.event.TouchEvent;

import java.io.IOException;

public class NavigationTabStrip extends Component implements PageSlider.PageChangedListener, Component.TouchEventListener, Component.DrawTask {

    // NTS constants
    private final static int INVALID_INDEX = -1;

    // Default variables
    private final static int DEFAULT_TITLE_SIZE = 0;

    // Title size offer to view height
    private final static float TITLE_SIZE_FRACTION = 0.35F;

    // Max and min fraction
    private final static float MIN_FRACTION = 0.0F;
    private final static float MAX_FRACTION = 1.0F;

    // NTS and strip bounds
    private RectFloat mBounds = null;
    private RectFloat mStripBounds = null;
    private final Rect mTitleBounds = new Rect();

    // Main paint
    private final Paint mStripPaint = new Paint() {
        {
            setStyle(Style.FILL_STYLE);
        }
    };

    // Paint for tav title
    private final Paint mTitlePaint = new Paint() {
        {
            setTextAlign(TextAlignment.CENTER);
        }
    };

    // Variables for animator
    private final AnimatorValue mAnimator = new AnimatorValue();
    public final ResizeInterpolator mResizeInterpolator = new ResizeInterpolator();
    public int mAnimationDuration;

    // NTS titles
    private String[] mTitles;

    // Variables for PageSlider
    private PageSlider mPageSlider;
    private PageSlider.PageChangedListener mOnPageChangeListener;
    private int mScrollState;

    // Variables for sizes
    private float mTabSize;
    // Tab title size and margin
    public int mTitleSize;
    //Tab title bold if possible with Paint
    public boolean mTitleBold;
    // Strip type and gravity
    public StripType mStripType;
    private StripGravity mStripGravity;
    // Corners radius for rect mode
    private int mStripWeight;
    private float mCornersRadius;

    // Indexes
    private int mLastIndex = INVALID_INDEX;
    private int mIndex = INVALID_INDEX;
    // General fraction value
    private float mFraction;

    // Coordinates of strip
    private float mStartStripX;
    private float mEndStripX;
    private float mStripLeft;
    private float mStripRight;

    // Detect if is bar mode or indicator pager mode
    private boolean mIsViewPagerMode;
    // Detect if we move from left to right
    private boolean mIsResizeIn;
    // Detect if we get action down event
    private boolean mIsActionDown;
    // Detect if we get action down event on strip
    private boolean mIsTabActionDown;
    // Detect when we set index from tab bar nor from ViewPager；true为tab点击；false为PageSlider滑动
    private boolean mIsSetIndexFromTabBar;

    // Color variables
    private Color mInactiveColor;
    private Color mActiveColor;
    //true: go to right  false: go to left
    private boolean mIsPositiveOrNegative = true;

    public NavigationTabStrip(Context context) {
        super(context);
    }

    public NavigationTabStrip(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    public NavigationTabStrip(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    public NavigationTabStrip(Context context, AttrSet attrSet, int resId) {
        super(context, attrSet, resId);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrSet) {

        setTouchEventListener(this);
        addDrawTask(this::onDraw);
        //Init NTS
        setStripColor(attrSet.getAttr("nts_color").get().getColorValue());
        setTitleSize(attrSet.getAttr("nts_size").get().getIntegerValue());
        setTitleBold(attrSet.getAttr("nts_bold").get().getBoolValue());
        setStripWeight(attrSet.getAttr("nts_weight").get().getDimensionValue());
        setStripFactor(attrSet.getAttr("nts_factor").get().getFloatValue());
        setStripType(attrSet.getAttr("nts_type").get().getIntegerValue());
        setStripGravity(attrSet.getAttr("nts_gravity").get().getIntegerValue());
        setInactiveColor(attrSet.getAttr("nts_inactive_color").get().getColorValue());
        setActiveColor(attrSet.getAttr("nts_active_color").get().getColorValue());
        setAnimationDuration(attrSet.getAttr("nts_animation_duration").get().getIntegerValue());
        setCornersRadius(attrSet.getAttr("nts_corners_radius").get().getDimensionValue());

        String[] titles = new String[0];
        try {
            titles = context.getResourceManager().getElement(ResourceTable.Strarray_tabs).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        setTitles(titles);

        // Init animator
        mAnimator.setCurveType(Animator.CurveType.LINEAR);
        mAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                updateIndicatorPosition(v);
            }
        });
        onMeasure();
    }

    protected void onMeasure() {
        // Get measure size
        final float width = DeviceUtils.getWindowWidthPx(getContext());
        final float height = getHeight();
        // Set bounds for NTS
        mBounds = new RectFloat(0.0F, 0.0F, width, height);
        if (mTitles.length == 0 || width == 0 || height == 0) return;
        // Get smaller side
        mTabSize = width / (float) mTitles.length;
        if (mTitleSize == DEFAULT_TITLE_SIZE)
            setTitleSize((int) ((height - mStripWeight) * TITLE_SIZE_FRACTION));

        // Set start position of strip for preview or on start
        if (!mIsViewPagerMode) {
            mIsSetIndexFromTabBar = true;
            // Set random in preview mode
            mIndex = 0;
            mStartStripX =
                    (mIndex * mTabSize) + (mStripType == StripType.POINT ? mTabSize * 0.5F : 0.0F);
            mEndStripX = mStartStripX;
            updateIndicatorPosition(MAX_FRACTION);
        }
    }

    public int getAnimationDuration() {
        return mAnimationDuration;
    }

    public void setAnimationDuration(final int animationDuration) {
        mAnimationDuration = animationDuration;
        mAnimator.setDuration(mAnimationDuration);
        resetScroller();
    }

    public String[] getTitles() {
        return mTitles;
    }

    public void setTitles(final String... titles) {
        for (int i = 0; i < titles.length; i++) titles[i] = titles[i].toUpperCase();
        mTitles = titles;
        postLayout();
    }

    public Color getStripColor() {
        return mStripPaint.getColor();
    }

    public void setStripColor(final Color color) {
        mStripPaint.setColor(color);
        invalidate();
    }

    public void setStripWeight(final int stripWeight) {
        mStripWeight = stripWeight;
        postLayout();
    }

    public StripGravity getStripGravity() {
        return mStripGravity;
    }

    private void setStripGravity(final int index) {
        switch (index) {
            case StripGravity.TOP_INDEX:
                setStripGravity(StripGravity.TOP);
                break;
            case StripGravity.BOTTOM_INDEX:
            default:
                setStripGravity(StripGravity.BOTTOM);
                break;
        }
    }

    public void setStripGravity(final StripGravity stripGravity) {
        mStripGravity = stripGravity;
        postLayout();
    }

    public StripType getStripType() {
        return mStripType;
    }

    private void setStripType(final int index) {
        switch (index) {
            case StripType.POINT_INDEX:
                setStripType(StripType.POINT);
                break;
            case StripType.LINE_INDEX:
            default:
                setStripType(StripType.LINE);
                break;
        }
    }

    public void setStripType(final StripType stripType) {
        mStripType = stripType;
        postLayout();
    }

    public float getStripFactor() {
        return mResizeInterpolator.getFactor();
    }

    public void setStripFactor(final float factor) {
        mResizeInterpolator.setFactor(factor);
    }

    public Color getActiveColor() {
        return mActiveColor;
    }

    public void setActiveColor(final Color activeColor) {
        mActiveColor = activeColor;
        invalidate();
    }

    public Color getInactiveColor() {
        return mInactiveColor;
    }

    public void setInactiveColor(final Color inactiveColor) {
        mInactiveColor = inactiveColor;
        invalidate();
    }

    public float getCornersRadius() {
        return mCornersRadius;
    }

    public void setCornersRadius(final float cornersRadius) {
        mCornersRadius = cornersRadius;
        invalidate();
    }

    public int getTitleSize() {
        return mTitleSize;
    }

    public void setTitleSize(final int titleSize) {
        mTitleSize = titleSize;
        mTitlePaint.setTextSize(titleSize);
        invalidate();
    }

    public boolean getTitleBold() {
        return mTitleBold;
    }

    public void setTitleBold(final boolean titleBold) {
        mTitleBold = titleBold;
        mTitlePaint.setFakeBoldText(titleBold);
        invalidate();
    }

    public void setPageSlider(final PageSlider pageSlider) {
        // Detect whether ViewPager mode
        if (pageSlider == null) {
            mIsViewPagerMode = false;
            return;
        }

        if (pageSlider.equals(mPageSlider)) return;
        if (mPageSlider != null) //noinspection deprecation
            mPageSlider.addPageChangedListener(null);
        if (pageSlider.getProvider() == null)
            throw new IllegalStateException("ViewPager does not provide adapter instance.");

        mIsViewPagerMode = true;
        mPageSlider = pageSlider;
        mPageSlider.addPageChangedListener(this);

        resetScroller();
        invalidate();
    }

    public void setPageSlider(final PageSlider pageSlider, int index) {
        setPageSlider(pageSlider);

        mIndex = index;
        if (mIsViewPagerMode) mPageSlider.setCurrentPage(index, true);
        invalidate();
    }

    // Reset scroller and reset scroll duration equals to animation duration
    private void resetScroller() {
        if (mPageSlider == null) return;
    }

    public void setOnPageChangeListener(final PageSlider.PageChangedListener listener) {
        mOnPageChangeListener = listener;
    }

    public int getTabIndex() {
        return mIndex;
    }

    public void setTabIndex(int index) {
        setTabIndex(index, false);
    }

    // Set tab index from touch or programmatically
    public void setTabIndex(int tabIndex, boolean isForce) {
        if (mAnimator.isRunning()) return;
        if (mTitles.length == 0) return;

        int index = tabIndex;
        boolean force = isForce;

        // This check gives us opportunity to have an non selected tab
        if (mIndex == INVALID_INDEX) force = true;

        // Snap index to tabs size
        index = Math.max(0, Math.min(index, mTitles.length - 1));

        mIsResizeIn = index < mIndex;
        mLastIndex = mIndex;
        mIndex = index;
        mIsSetIndexFromTabBar = true;
        if (mIsViewPagerMode) {
            if (mPageSlider == null) throw new IllegalStateException("PageSlider is null.");
            mPageSlider.setCurrentPage(index, !force);
        }

        // Set startX and endX for animation, where we animate two sides of rect with different interpolation
        mStartStripX = mStripLeft;
        mEndStripX = (mIndex * mTabSize) + (mStripType == StripType.POINT ? mTabSize * 0.5F : 0.0F);

        /// If it force, so update immediately, else animate
        // This happens if we set index onCreate or something like this
        // You can use force param or call this method in some post()
        if (force) {
            updateIndicatorPosition(MAX_FRACTION);
        } else mAnimator.start();
    }

    // Deselect active index and reset pointer
    public void deselect() {
        mLastIndex = INVALID_INDEX;
        mIndex = INVALID_INDEX;
        mStartStripX = INVALID_INDEX * mTabSize;
        mEndStripX = mStartStripX;
        updateIndicatorPosition(MIN_FRACTION);
    }

    private void updateIndicatorPosition(final float fraction) {
        // Update general fraction
        mFraction = fraction;
        // Set the strip left side coordinate

        float result = mEndStripX - mStartStripX;
        if (!mIsPositiveOrNegative) {
            mStripRight =
                    mStartStripX + (mResizeInterpolator.getResizeInterpolation(fraction, mIsResizeIn) * result) + 40;
            // Set the strip right side coordinate
            mStripLeft =
                    (mStartStripX + (mStripType == StripType.LINE ? mTabSize : mStripWeight)) +
                            (mResizeInterpolator.getResizeInterpolation(fraction, !mIsResizeIn) * result);
        } else {
            mStripLeft =
                    mStartStripX + (mResizeInterpolator.getResizeInterpolation(fraction, mIsResizeIn) * result);
            // Set the strip right side coordinate
            mStripRight =
                    (mStartStripX + (mStripType == StripType.LINE ? mTabSize : mStripWeight)) +
                            (mResizeInterpolator.getResizeInterpolation(fraction, !mIsResizeIn) * result);
        }
        // Update NTS
        invalidate();
    }

    // Update NTS
    private void notifyDataSetChanged() {
        postLayout();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        // Return if animation is running
        if (mAnimator.isRunning()) return true;
        // If is not idle state, return
        if (mScrollState != PageSlider.SLIDING_STATE_IDLE) return true;

        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mIsDown = true;
                // Action down touch
                mIsActionDown = true;
                if (!mIsViewPagerMode) break;
                // Detect if we touch down on tab, later to move
                mIsTabActionDown = (int) (touchEvent.getPointerPosition(touchEvent.getIndex()).getX() / mTabSize) == mIndex;
                break;
            case TouchEvent.POINT_MOVE:
                // If tab touched, so move
                if (mIsTabActionDown) {
                    mPageSlider.setCurrentPage((int) (touchEvent.getPointerPosition(touchEvent.getIndex()).getX() / mTabSize), true);
                    break;
                }
                if (mIsActionDown) break;
            case TouchEvent.PRIMARY_POINT_UP:
                // Press up and set tab index relative to current coordinate
                mIsPositiveOrNegative = true;
                setTabIndex((int) (touchEvent.getPointerPosition(touchEvent.getIndex()).getX() / mTabSize));
            case TouchEvent.CANCEL:
            default:
                // Reset action touch variables
                mIsTabActionDown = false;
                mIsActionDown = false;
                break;
        }

        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        // Set bound of strip
        mStripBounds = new RectFloat(
                mStripLeft - (mStripType == StripType.POINT ? mStripWeight * 0.5F : 0.0F),
                mStripGravity == StripGravity.BOTTOM ? mBounds.getHeight() - mStripWeight : 0.0F,
                mStripRight - (mStripType == StripType.POINT ? mStripWeight * 0.5F : 0.0F),
                mStripGravity == StripGravity.BOTTOM ? mBounds.getHeight() : mStripWeight
        );
        // Draw strip
        if (mCornersRadius == 0) canvas.drawRect(mStripBounds, mStripPaint);
        else canvas.drawRoundRect(mStripBounds, mCornersRadius, mCornersRadius, mStripPaint);

        // Draw tab titles
        for (int i = 0; i < mTitles.length; i++) {
            final String title = mTitles[i];

            final float leftTitleOffset = (mTabSize * i) + (mTabSize * 0.5F);

            final float topTitleOffset = (mBounds.getHeight() - mStripWeight) * 0.5F +
                    mTitleBounds.getHeight() * 0.5F - mTitleBounds.bottom;

            // Get interpolated fraction for left last and current tab
            final float interpolation = mResizeInterpolator.getResizeInterpolation(mFraction, true);
            final float lastInterpolation = mResizeInterpolator.getResizeInterpolation(mFraction, false);

            // Check if we handle tab from touch on NTS or from ViewPager
            // There is a strange ic of ViewPager onPageScrolled method, so it is
            if (mIsSetIndexFromTabBar) { //tab点击
                if (mIndex == i) updateCurrentTitle(interpolation);
                else if (mLastIndex == i) updateLastTitle(lastInterpolation);
                else updateInactiveTitle();
            } else { //PageSlider滑动
                if (mIsPositiveOrNegative) {
                    if (i == mIndex + 1) {
                        updateCurrentTitle(interpolation);
                    } else {
                        updateInactiveTitle();
                        updateLastTitle(lastInterpolation);
                    }
                } else {
                    if (i == mIndex) {
                        updateCurrentTitle(interpolation);
                    } else {
                        updateInactiveTitle();
                        updateLastTitle(lastInterpolation);
                    }
                }
            }
            canvas.drawText(mTitlePaint, title, leftTitleOffset, topTitleOffset + (mStripGravity == StripGravity.TOP ? mStripWeight : 0.0F));
        }
    }

    // Method to transform current fraction of NTS and position
    private void updateCurrentTitle(final float interpolation) {
        mTitlePaint.setColor(mActiveColor);
    }

    // Method to transform last fraction of NTS and position
    private void updateLastTitle(final float lastInterpolation) {
        mTitlePaint.setColor(mInactiveColor);
    }

    // Method to transform others fraction of NTS and position
    private void updateInactiveTitle() {
        mTitlePaint.setColor(mInactiveColor);
    }

    @Override
    public void onPageChosen(int i) {
        // This method is empty, because we call onPageSelected() when scroll state is idle
    }

    private boolean mIsDown = true;

    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
        if (positionOffset >= 1) return;

        if (mOnPageChangeListener != null)
            mOnPageChangeListener.onPageSliding(position, positionOffset, positionOffsetPixels);
        // If we animate, don`t call this
        if (!mIsSetIndexFromTabBar) {
            mIsResizeIn = position < mIndex;
            mLastIndex = mIndex;
            if (positionOffsetPixels < 0) {
                mIsPositiveOrNegative = false;
                mIndex = position - 1;
                mStartStripX = ((position * mTabSize) + (mStripType == StripType.POINT ? mTabSize * 0.43F : 0.0F));
                mEndStripX = mStartStripX - mTabSize;
            } else {
                mIsPositiveOrNegative = true;
                mIndex = position;
                mStartStripX = (position * mTabSize) + (mStripType == StripType.POINT ? mTabSize * 0.5F : 0.0F);
                mEndStripX = mStartStripX + mTabSize;
            }
            updateIndicatorPosition(positionOffset);
            mIsDown = false;
        }

        // Stop scrolling on animation end and reset values
        if (!mAnimator.isRunning() && mIsSetIndexFromTabBar) {
            mFraction = MIN_FRACTION;
            mIsSetIndexFromTabBar = false;
        }

    }

    @Override
    public void onPageSlideStateChanged(int state) {
        // If VP idle, reset to MIN_FRACTION
        mScrollState = state;
    }

    // Resize interpolator to create smooth effect on strip according to inspiration design
    // This is like improved accelerated and decelerated interpolator
    public class ResizeInterpolator implements Interpolator {

        // Spring factor
        public float mFactor;
        // Check whether side we move
        private boolean mResizeIn;

        public float getFactor() {
            return mFactor;
        }

        public void setFactor(final float factor) {
            mFactor = factor;
        }

        @Override
        public float getInterpolation(final float input) {
            if (mResizeIn) return (float) (1.0F - Math.pow((1.0F - input), 2.0F * mFactor));
            else return (float) (Math.pow(input, 2.0F * mFactor));
        }

        public float getResizeInterpolation(final float input, final boolean resizeIn) {
            mResizeIn = resizeIn;
            return getInterpolation(input);
        }
    }

    // NTS strip type
    public enum StripType {
        LINE, POINT;

        public final static int LINE_INDEX = 0;
        public final static int POINT_INDEX = 1;
    }

    // NTS strip gravity
    public enum StripGravity {
        BOTTOM, TOP;

        private final static int BOTTOM_INDEX = 0;
        private final static int TOP_INDEX = 1;
    }

    // Out listener for selected index
    public interface OnTabStripSelectedIndexListener {
        void onStartTabSelected(final String title, final int index);

        void onEndTabSelected(final String title, final int index);
    }
}
