/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gigamole.navigationtabstrip;

import com.hos.navigation.MainAbility;
import com.hos.navigation.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.ability.delegation.IAbilityMonitor;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import org.junit.Assert;
import org.junit.Test;

public class TabIndexTest {

    @Test
    public void tabIndex() {

        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        IAbilityMonitor abilityMonitor = sAbilityDelegator.addAbilityMonitor(MainAbility.class.getCanonicalName());
        Intent intent = new Intent();
        Operation operation =
                new Intent.OperationBuilder().withBundleName(context.getBundleName()).withAbilityName(MainAbility.class).build();
        intent.setOperation(operation);
        sAbilityDelegator.startAbilitySync(intent);
        Ability ability = abilityMonitor.waitForAbility();

        NavigationTabStrip navigationTabStrip = (NavigationTabStrip) ability.findComponentById(ResourceTable.Id_top);
        navigationTabStrip.setTabIndex(1, false);
        Assert.assertSame(navigationTabStrip.getTabIndex(), 1);
    }
}