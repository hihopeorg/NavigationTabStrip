/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos.navigation;


import com.gigamole.navigationtabstrip.NavigationTabStrip;
import ohos.agp.components.PageSlider;
import org.junit.Assert;
import org.junit.Test;

import static com.hos.navigation.EventHelper.waitForActive;


public class NavigationTabStripCenterTest {

    @Test
    public void navigationTabStripCenter() {
        MainAbility ability = EventHelper.startAbility(MainAbility.class);
        waitForActive(ability, 2000);
        NavigationTabStrip navigationTabStrip = (NavigationTabStrip) ability.findComponentById(ResourceTable.Id_center);
        PageSlider pageSlider = (PageSlider) ability.findComponentById(ResourceTable.Id_pageSlider);
        navigationTabStrip.setPageSlider(pageSlider, 0);
        Assert.assertSame(pageSlider.getCurrentPage(), 0);
        Assert.assertSame(navigationTabStrip.mTitleSize, 50);
        Assert.assertSame(navigationTabStrip.mTitleBold, false);
    }

}