/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hos.navigation.slice;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.hos.navigation.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private PageSlider mPageSlider;
    private NavigationTabStrip mNavigationTabStripTop;
    private NavigationTabStrip mNavigationTabStripCenter;
    private NavigationTabStrip mNavigationTabStripBottom;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageSlider);
        mNavigationTabStripTop = (NavigationTabStrip) findComponentById(ResourceTable.Id_top);
        mNavigationTabStripCenter = (NavigationTabStrip) findComponentById(ResourceTable.Id_center);
        mNavigationTabStripBottom = (NavigationTabStrip) findComponentById(ResourceTable.Id_bottom);

        LayoutScatter scatter = LayoutScatter.getInstance(getContext());
        DependentLayout page0 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_0, null, false);
        DependentLayout page1 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_1, null, false);
        DependentLayout page2 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_2, null, false);
        DependentLayout page3 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_3, null, false);
        final List<Component> pages = new ArrayList<>();
        pages.add(page0);
        pages.add(page1);
        pages.add(page2);
        pages.add(page3);

        mPageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pages.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pages.get(i));
                return pages.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pages.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        mNavigationTabStripTop.setTabIndex(1, true);
        mNavigationTabStripCenter.setPageSlider(mPageSlider, 0);
        mNavigationTabStripBottom.setTabIndex(2, true);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
