# NavigationTabStrip

**本项目是基于开源项目NavigationTabStrip进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/Devlight/NavigationTabStrip ）追踪到原项目版本**

#### 项目介绍

- 项目名称：具有平滑交互的导航条
- 所属系列：ohos的第三方组件适配移植
- 功能：具有平滑交互的导航条，Tab切换以及PageSlider滑动切换
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/Devlight/NavigationTabStrip
- 原项目基线版本：v1.0.4 , sha1:e7eff1944a9f2258f84415f5f5fa4210c5868d0c
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

<img src="preview/preview.gif"/>

#### 安装教程

方法1.

1. 编译har包NavigationTabStrip.har。

2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

  ```
  dependencies {
   implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
  ……
  }
  ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

  ```
  repositories {
   maven {
       url 'http://106.15.92.248:8081/repository/Releases/' 
   }
  }
  ```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

  ```
  dependencies {
   implementation 'com.github.devlight.navigationtabstrip.ohos:navigationtabstrip:1.0.1'
  }
  ```

#### 使用说明

1. 布局文件定义，提供控件：NavigationTabStrip
```xml
<StackLayout
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:alignment="center"
        ohos:background_element="$color:red_light">

        <com.gigamole.navigationtabstrip.NavigationTabStrip
            ohos:id="$+id:top"
            ohos:height="60vp"
            ohos:width="match_parent"
            app:nts_active_color="$color:blue"
            app:nts_animation_duration="500"
            app:nts_bold="true"
            app:nts_color="$color:blue"
            app:nts_corners_radius="10vp"
            app:nts_factor="2.5"
            app:nts_gravity="0"
            app:nts_inactive_color="$color:black"
            app:nts_size="50"
            app:nts_type="0"
            app:nts_weight="5vp"/>
    </StackLayout>
```

2. 结合PageSlider使用
```xml
<DirectionalLayout
        ohos:height="0vp"
        ohos:width="match_parent"
        ohos:background_element="$color:white"
        ohos:weight="1">

        <StackLayout
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:background_element="#FF252E39"
            ohos:layout_alignment="center">

            <com.gigamole.navigationtabstrip.NavigationTabStrip
                ohos:id="$+id:center"
                ohos:height="60vp"
                ohos:width="match_parent"
                app:nts_tabs="strarray:tabs"
                app:nts_active_color="$color:white"
                app:nts_animation_duration="500"
                app:nts_bold="false"
                app:nts_color="#3CDEDD"
                app:nts_corners_radius="10vp"
                app:nts_factor="0.8"
                app:nts_gravity="0"
                app:nts_inactive_color="#81FFFFFF"
                app:nts_size="50"
                app:nts_type="1"
                app:nts_weight="7vp"/>
        </StackLayout>

        <PageSlider
            ohos:id="$+id:pageSlider"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="$color:gray"/>
    </DirectionalLayout>
```
```java(详见Demo中的MainAbilitySlice类)
LayoutScatter scatter = LayoutScatter.getInstance(getContext());
        DependentLayout page0 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_0, null, false);
        DependentLayout page1 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_1, null, false);
        DependentLayout page2 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_2, null, false);
        DependentLayout page3 = (DependentLayout) scatter.parse(ResourceTable.Layout_layout_page_3, null, false);
        final List<Component> pages = new ArrayList<>();
        pages.add(page0);
        pages.add(page1);
        pages.add(page2);
        pages.add(page3);

        mPageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pages.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pages.get(i));
                return pages.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pages.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        mNavigationTabStripCenter.setPageSlider(mPageSlider, 0);
```

#### 版本迭代
- v1.0.1

